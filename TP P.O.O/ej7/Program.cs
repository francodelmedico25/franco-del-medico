﻿using System;

namespace ej7
{
    class Raices
    {
        private double a;
        private double b;
        private double c;

        public string obtenerRaices()
        {
            double raiz1 = ((this.b * 1) + Math.Sqrt((Math.Pow(this.b, 2)) - ((4 * this.a) * this.c))) / (2 * this.a);
            double raiz2 = ((this.b * 1) - Math.Sqrt((Math.Pow(this.b, 2)) - ((4 * this.a) * this.c))) / (2 * this.a);
            return String.Format("Raiz 1: {0} \n Raiz 2: {1}", raiz1, raiz2);

        }
        public void obtenerRaiz()
        {
            if (this.getDiscriminante() == 0)
            {
                double raiz = (this.b * -1) / (2 * this.a);
                Console.WriteLine("La unica raiz de la funcion es: {0}", raiz);
            }
            else
            {
                Console.WriteLine("El sistema no tiene raices o tiene mas de una");
            }

        }
        public double getDiscriminante()
        {
            return (Math.Pow(this.b, 2) - (4 * this.a * this.c));
        }
        public bool tieneRaices()
        {
            return (this.getDiscriminante() >= 0);

        }
        public bool tieneRaiz()
        {
            return (this.getDiscriminante() == 0);

        }
        public void calcular()
        {
            if (this.tieneRaices())
            {
                if (this.tieneRaiz())
                {
                    this.obtenerRaiz();
                }
                else
                {
                    this.obtenerRaices();
                }
            }
            else
            {
                Console.WriteLine("La funcion no tiene raices.");
            }
        }
    }

}
