﻿using System;

namespace ej2
{
    class Persona
    {
        private string nombre = "";
        private int edad = 0;
        private int DNI;
        private char sexo = 'H';
        private float peso = 0;
        private float altura = 0;
        System.Random random = new System.Random();
        public Persona()
        {
            this.generarDNI();
        }
        public Persona(string nombre, int edad, char sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
        }
        public Persona(string nombre, int edad, int DNI, char sexo, float peso, float altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.DNI = DNI;
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;
        }
        public int calcularIMC()
        {
            float calculo = (this.peso / (float)Math.Pow(this.altura, 2));
            if (calculo < 20)
            {
                return -1;
            }
            else if (calculo >= 20 && calculo <= 25)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public bool esMayorDeEdad()
        {
            if (this.edad >= 18)
            {
                return true;
            }
            return false;
        }
        private void generarDNI()
        {
            this.DNI = random.Next(10000000, 99999999);
        }
        public string Nombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            set
            {
                this.edad = value;
            }
        }
        public char Sexo
        {
            set
            {
                this.sexo = value;
            }
        }
        public float Peso
        {
            set
            {
                this.peso = value;
            }
        }
        public float Altura
        {
            set
            {
                this.altura = value;
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese un nombre: ");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese una edad: ");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el sexo. 'H' para hombre, 'M' para mujer:");
            char sexo = Console.ReadLine()[0];
            Console.WriteLine("Ingrese un valor para el peso:");
            float peso = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese un valor para la altura:");
            float altura = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese DNI:");
            int dni = int.Parse(Console.ReadLine());

            Persona[] personas = new Persona[3];

            personas[0] = new Persona(nombre, edad, dni, sexo, peso, altura);
            personas[1] = new Persona(nombre, edad, sexo);
            personas[2] = new Persona();
            personas[2].Nombre = "Juan";
            personas[2].Edad = 15;
            personas[2].Sexo = 'H';
            personas[2].Altura = 161.3F;
            personas[2].Peso = 63.5F;

            foreach (Persona p in personas)
            {
                switch (p.calcularIMC())
                {
                    case -1:
                        Console.WriteLine("Esta por debajo de su peso ideal");
                        break;
                    case 0:
                        Console.WriteLine("Peso ideal");
                        break;
                    case 1:
                        Console.WriteLine("Tiene sobrepeso");
                        break;
                }
            }
        }
    }
}
