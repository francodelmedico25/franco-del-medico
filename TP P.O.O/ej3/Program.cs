﻿using System;

namespace ej3
{
    class Password
    {
        private int longitud = 8;
        private string contraseña;

        public Password() { }
        public Password(int longitud)
        {
            this.longitud = longitud;
            generarPassword();
        }
        public void generarPassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[this.longitud];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            this.contraseña = new String(stringChars);
        }
        public bool esFuerte()
        {
            int numeros = 0, mayus = 0, minus = 0;
            foreach (char c in this.contraseña)
            {
                if ((byte)c >= 65 && (byte)c <= 90)
                {
                    mayus++;
                }
                else if ((byte)c >= 97 && (byte)c <= 122)
                {
                    minus++;
                }
                else if ((byte)c >= 48 && (byte)c <= 57)
                {
                    numeros++;
                }
            }
            if (numeros > 5 && mayus > 2 && minus > 1)
            {
                return true;
            }
            return false;
        }
        public string Contraseña
        {
            get
            {
                return this.contraseña;
            }
        }
        public int Longitud
        {
            get
            {
                return this.longitud;
            }
            set
            {
                this.longitud = value;
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la cantidad de contraseñas a generar");
            int cant = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la longitud de las contraseñas por teclado");
            int longitud = int.Parse(Console.ReadLine());
            Password[] contraseñas = new Password[cant];
            bool[] es_fuerte = new bool[cant];
            for (int i = 0; i < cant; i++)
            {
                contraseñas[i] = new Password(longitud);
                es_fuerte[i] = contraseñas[i].esFuerte();
                Console.WriteLine("{0} {1}", contraseñas[i], es_fuerte[i]);
            }
        }
    }
}
