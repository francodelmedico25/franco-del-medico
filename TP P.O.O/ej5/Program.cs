﻿using System;

namespace ej5
{
    interface Entregable
    {
        void entregar();
        void devolver();
        bool isEntregado();
        Entregable compareTo(Entregable o);

    }
    class Serie : Entregable
    {
        private string titulo = "";
        private int numero_temporadas = 3;
        private string genero = "";
        private string creador = "";
        private bool entregado = false;
        public Serie() { }
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }
        public Serie(string titulo, string creador, string genero, int numero_temporadas)
        {
            this.creador = creador;
            this.titulo = titulo;
            this.genero = genero;
            this.numero_temporadas = numero_temporadas;
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
        }
        public int Numero_temporadas
        {
            get
            {
                return this.numero_temporadas;
            }
        }
        public string Genero
        {
            get
            {
                return this.genero;
            }
        }
        public string Creador
        {
            get
            {
                return this.creador;
            }
        }
        public string setTitulo
        {
            set
            {
                this.titulo = value;
            }
        }
        public int setNumero_temporadas
        {
            set
            {
                this.numero_temporadas = value;
            }
        }
        public string setGenero
        {
            set
            {
                this.genero = value;
            }
        }
        public string setCreador
        {
            set
            {
                this.creador = value;
            }
        }
        public void devolver()
        {
            this.entregado = false;
        }
        public void entregar()
        {
            this.entregado = true;
        }
        public bool isEntregado()
        {
            return this.entregado;
        }
        public Entregable compareTo(Entregable o)
        {
            if(o is Serie)
            {
                Serie z = (Serie)o;
                if(z.Numero_temporadas > this.numero_temporadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if(o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if(z.Horas_estimadas > this.numero_temporadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }

    class Videojuego : Entregable
    {
        private string titulo = "";
        private int horas_estimadas = 10;
        private string genero = "";
        private string compañia = "";
        private  bool entregado = false;

        public Videojuego() { }

        public Videojuego (string titulo, int horas_estimadas)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
        }
        
        public Videojuego(string titulo, string genero, string compañia, int horas_estimadas)
        {
            this.titulo = titulo;
            this.genero = genero;
            this.compañia = compañia;
            this.horas_estimadas = horas_estimadas;
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
        }
        public string Genero
        {
            get
            {
                return this.genero;
            }
        }
        public string Compañia
        {
            get
            {
                return this.compañia;
            }
        }
        public int Horas_estimadas
        {
            get
            {
                return this.horas_estimadas;
            }
        }
        public string setTitulo
        {
            set
            {
                this.titulo = value;
            }
        }
        public string setGenero
        {
            set
            {
                this.genero = value;
            }
        }
        public string setCompañia
        {
            set
            {
                this.compañia = value;
            }
        }
        public int setHoras_estimadas
        {
            set
            {
                this.horas_estimadas = value;
            }
        }
        public void devolver()
        {
            this.entregado = false;
        }
        public void entregar()
        {
            this.entregado = true;
        }
        public bool isEntregado()
        {
            return this.entregado;
        }
        public Entregable compareTo(Entregable o)
        {
            if (o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if (z.Horas_estimadas > this.horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if (o is Serie)
            {
                Serie z = (Serie)o;
                if (z.Numero_temporadas > this.horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
                
        
        }
    }

    class Ejecutable
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];
            Videojuego[] videojuegos = new Videojuego[5];
            series[0] = new Serie("strage", "juan");
            series[1] = new Serie("strageee","bestia","mousntro",321);
            series[2] = new Serie("100","z");
            series[3] = new Serie("Los vengadores","Stan Lee");
            series[4] = new Serie("don quijote","papa noel");
            videojuegos[0] = new Videojuego("lol",412);
            videojuegos[1] = new Videojuego("Among us",110);
            videojuegos[2] = new Videojuego("valorant",344);
            videojuegos[3] = new Videojuego("gta","gente clasica","robert kiyosaki",10);
            videojuegos[4] = new Videojuego("minecraft",21);
            series[4].entregar();
            videojuegos[2].entregar();
            videojuegos[1].entregar();
            series[3].entregar();
            series[0].entregar();
            int juegos_entregados = 0, series_entregadas = 0;
            Serie mas_larga = series[0];
            Videojuego mas_largo = videojuegos[0];
            for (int w = 0; w < 5; w++)
            {
                if (series[w].isEntregado())
                {
                    series_entregadas++;
                    series[w].devolver();
                }
                if (videojuegos[w].isEntregado())
                {
                    juegos_entregados++;
                    videojuegos[w].devolver();
                }
                if (series[w].Numero_temporadas > mas_larga.Numero_temporadas)
                {
                    mas_larga = series[w];
                }
                if (videojuegos[w].Horas_estimadas > mas_largo.Horas_estimadas)
                {
                    mas_largo = videojuegos[w];
                }
            }
            Console.WriteLine("Habia {0} videojuegos prestados y {1} series prestadas.", juegos_entregados, series_entregadas);
            Console.WriteLine("La serie mas larga es: {0}", mas_larga.Titulo);
            Console.WriteLine("El juego con mas horas estimadas es {0}", mas_largo.Titulo);
        }
    }
}
