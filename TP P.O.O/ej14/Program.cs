﻿using System;
using System.Collections.Generic;
namespace ej14
{
    class Bebidas
    {
        protected int identificador;
        protected float cantLitros;
        protected float precio;
        protected string marca;
        public Bebidas(int identificador,int cantLitros, float precio, string marca)
        {
            this.identificador = identificador;
            this.cantLitros = cantLitros;
            this.precio = precio;
            this.marca = marca;
        }
        public float getPrecio
        {
            get
            {
                return this.precio;
            }
        }
        public string getMarca
        {
            get
            {
                return this.marca;
            }
        }
        public int getIdentificador
        {
            get
            {
                return this.identificador;
            }
        }
        public string toString()
        {
            return String.Format("id = {0} cantidad de litros = {1} precio = {2} marca= {3}", this.identificador, this.cantLitros, this.precio, this.marca);
        }

    }

    class BebidasAzucaradas : Bebidas
    {
        private float porcentajeAzucar;
        private bool promocion;

        public BebidasAzucaradas(int identificador, int cantLitros, int precio, string marca,float porcentajeAzucar, bool promocion) : base(identificador,cantLitros,precio,marca)
        {
            this.porcentajeAzucar = porcentajeAzucar;
            this.promocion = promocion;
        }

        public float PorcentajeAzucar()
        {
            return porcentajeAzucar;
        }
        public bool TienePromocion()
        {
            return promocion;
        }
        public double PrecioPromocion()
        {
            if (TienePromocion())
            {
                return PrecioPromocion() * 0.9;
            }
            else
            {
                return PrecioPromocion();
            }
        }
    }
    
    class AguaMineral : Bebidas
    {
        private string manantial;

        public AguaMineral(int identificador,int cantLitros, int precio, string marca,string manantial) : base(identificador,cantLitros, precio, marca)
        {
            this.manantial = manantial;
        }
        public new string toString()
        {
            return String.Format("{0} y su origen es {1}", base.toString(), this.manantial);
        }

    }

    class Almacen
    {
        private List<List<Bebidas>> estanteria = new List<List<Bebidas>>();

        public Almacen(List<List<Bebidas>> estanteria)
        {
            this.estanteria = estanteria;
        }
        public float CalcularPrecioBebidas()
        {
            float precioTotal = 0;
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b != null)
                    {
                        precioTotal += b.getPrecio;
                    }
                }
            }
            return precioTotal;

        }
        public float CalcularPrecioTotalMarcaBebida(string marca)
        {
            float precioTotal = 0;
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b != null)
                    {
                        if (b.getMarca == marca)
                        {
                            precioTotal += b.getPrecio;
                        }
                    }
                }
            }
            return precioTotal;
        }
        public float CalcularPrecioTotalEstanteria(int num_estanteria)
        {
            float precioTotal = 0;
            if (num_estanteria > this.estanteria.Count)
            {
                return 0;
            }
            else if(num_estanteria == 0)
            {
                return 0;
            }
            foreach (List<Bebidas> l in this.estanteria)
            {
                if (l[num_estanteria - 1] != null)
                {
                    precioTotal += l[num_estanteria - 1].getPrecio;
                }
            }
            return precioTotal;

        }
        public bool AgregarProducto(Bebidas a)
        {
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b.getIdentificador == a.getIdentificador)
                    {
                        return false;
                    }
                }
            }
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b == null)
                    {
                        this.estanteria[this.estanteria.IndexOf(l)][l.IndexOf(b)] = a;
                        return true;
                    }
                }
            }
            return false;

        }
        public bool EliminarProducto(int id)
        {
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b != null)
                    {
                        if (b.getIdentificador == id)
                        {
                            this.estanteria[this.estanteria.IndexOf(l)][l.IndexOf(b)] = null;
                            return true;
                        }
                    }
                }
            }
            return false;

        }
        public void MostrarInfo()
        {
            foreach (List<Bebidas> l in this.estanteria)
            {
                foreach (Bebidas b in l)
                {
                    if (b is AguaMineral)
                    {
                        AguaMineral a = (AguaMineral)b;
                        Console.WriteLine(a.toString());
                    }
                    else if (b is BebidasAzucaradas)
                    {
                        BebidasAzucaradas a = (BebidasAzucaradas)b;
                        Console.WriteLine(a.toString());
                    }
                }
            }

        }





    }
}
