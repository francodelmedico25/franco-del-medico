﻿using System;

namespace ej6
{
    public class Libro
    {
        private int iSBN;
        private string titulo;
        private string autor;
        private int numero_paginas;

        public Libro(int iSBN, string titulo, string autor, int numero_paginas)
        {
            this.iSBN = iSBN;
            this.titulo = titulo;
            this.autor = autor;
            this.numero_paginas = numero_paginas;
        }
        public int ISBN
        {
            get
            {
                return this.iSBN;
            }
            set
            {
                this.iSBN = value;
            }
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public string Autor
        {
            get
            {
                return this.autor;
            }
            set
            {
                this.autor = value;
            }
        }
        public int Numero_paginas
        {
            get
            {
                return this.numero_paginas;
            }
            set
            {
                this.numero_paginas = value;
            }
        }
        public string toString()
        {
            return "El libro " + titulo + " con ISBN " + ISBN + "" + " creado por el autor " + autor + " tiene " + numero_paginas + " páginas";
        }
    }

    public class Ejecutable
    {
        public static void Main(string[] args)
        {
            Libro libro1 = new Libro(32132, "Los juegos del hambre", "Suzanne Collins", 396);
            Libro libro2 = new Libro(32138, "La histeria", "Freud", 226);

            int Paginas1 = libro1.Numero_paginas;
            string TituloLibro1 = libro1.Titulo;

            int Paginas2 = libro2.Numero_paginas;
            string TituloLibro2 = libro2.Titulo;

            if (Paginas1 < Paginas2)
            {
                Console.WriteLine("El libro " + TituloLibro2 + " tiene mas paginas ");
            }
            else if (Paginas1 > Paginas2)
            {
                Console.WriteLine("El libro " + TituloLibro1 + " tiene mas paginas ");
            }
            else
            {
                Console.WriteLine("Tienen las mismas paginas");
            }
        }
    }
}
