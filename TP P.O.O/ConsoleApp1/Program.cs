﻿using System;

namespace ConsoleApp1
{
    class Cuenta 
    {
        private string titular;
        private double cantidad;
       Cuenta(string titular,double cant)
        {
            this.titular = titular;
            this.cantidad = cant;
        }
       Cuenta(string titular)
        {
            this.titular = titular;
            this.cantidad = 0;
        }
        public string Titular
        {
            get
            {
                return this.titular;
            }
            set
            {
                this.titular = value;
            }           
        }
        public double Cantidad
        {
            get
            {
                return this.cantidad;
            }
            set
            {
                this.cantidad = value;
            }
        }
        public void ingresar(double valor)
        {
            this.cantidad += valor;
        }
        public void retirar(double cant)
        {
            this.cantidad -= cant;
            if (this.cantidad < 0)
            {
                this.cantidad = 0;
            }
        }
    }
}

