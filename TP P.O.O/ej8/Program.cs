﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;


namespace ej8
{
    abstract class Persona
    {
        protected string nombre;
        protected char sexo;
        protected int edad;
        protected bool asistencia;

        public Persona(string nombre, char sexo, int edad)
        {
            this.nombre = nombre;
            this.sexo = sexo;
            this.edad = edad;
            this.disponibilidad();
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public char getSexo
        {
            get
            {
                return this.sexo;
            }
        }
        public int getEdad
        {
            get
            {
                return this.edad;
            }
        }
        public bool getAsistencia
        {
            get
            {
                return this.asistencia;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public char setSexo
        {
            set
            {
                this.sexo = value;
            }
        }
        public int setEdad
        {
            set
            {
                this.edad = value;
            }
        }
        public bool setAsistencia
        {
            set
            {
                this.asistencia = value;
            }
        }
        public abstract void disponibilidad();


    }

    class Estudiante : Persona
    {
        private int nota;
        public Estudiante(string nombre, char sexo, int edad, int nota) : base(nombre, sexo, edad)
        {
            this.nota = nota;
        }
        public int getNota
        {
            get
            {
                return this.nota;
            }
        }
        public int set
        {
            set
            {
                this.nota = value;
            }
        }
        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 50) ? false : true;
        }
        public string toString()
        {
            return string.Format("Nombre: {0}, Sexo: {1}, Nota: {2}", this.nombre, this.sexo, this.nota);

        }
    }

    class Profesor : Persona
    {
        private string materia;
        public Profesor(string nombre, char sexo, int edad, string materia) : base(nombre, sexo, edad)
        {
            this.materia = materia;
        }
        public string getMateria
        {
            get
            {
                return this.materia;
            }
        }
        public string setMateria
        {
            set
            {
                this.materia = value;
            }
        }
        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 20) ? false : true;
        }




    }

    class Aula
    {
        private int id;
        private Profesor profesor;
        private List<Estudiante> estudiantes;
        private string materia;

        public Aula(int id, List<string> nombres, List<char> sexo, List<int> edades, List<int> notas)
        {
            for (int i = 0; i < nombres.Count; i++)
            {
                estudiantes.Add(new Estudiante(nombres[i], sexo[i], edades[i], notas[i]));
            }
        }
        public bool asistenciaAlumnos()
        {
            int presentes = 0;
            foreach (Estudiante e in estudiantes)
            {
                e.disponibilidad();
                if (e.getAsistencia)
                {
                    presentes++;
                }
            }
            return (presentes >= Math.Round((decimal)estudiantes.Count / 2));
        }
        public bool darClase()
        {
            profesor.disponibilidad();
            if (!profesor.getAsistencia)
            {
                return false;
            }
            if (profesor.getMateria != this.materia)
            {
                Console.WriteLine("No se puede dar clase. El profesor es de otra materia.");
                return false;
            }
            if (!this.asistenciaAlumnos())
            {
                Console.WriteLine("No se puede dar clase. Asistencia insuficiente.");
                return false;
            }
            Console.WriteLine("Se puede dar clases.");
            return true;
        }
        public void notas()
        {
            int cm = 0;
            int ch = 0;
            foreach (Estudiante e in this.estudiantes)
            {
                if (e.getSexo == 'H')
                {
                    ch++;
                }
                else if (e.getSexo == 'M')
                {
                    cm++;
                }
                Console.WriteLine(e.toString());
            }
            Console.WriteLine("Hay {0} chicos y {1} chicas aprobados/as", ch, cm);
        }
    }

    class Ejecutable
    {
        static void Main(string[] args)
        {
            Aula aula = new Aula(1, new List<string>() { "juan", "pedro", "anto" }, new List<char>() { 'H', 'H', 'M' }, new List<int>() { 11, 12, 10 }, new List<int>() { 5, 10, 8 });
            if (aula.darClase())
            {
                aula.notas();
            }
        }

    }

}




